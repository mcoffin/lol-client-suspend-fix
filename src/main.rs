#[macro_use] extern crate clap;
extern crate env_logger;
extern crate log;
extern crate regex;
extern crate nix;
extern crate libc;
extern crate thiserror;

pub mod procfs;
pub(crate) mod timeout;
pub mod tailrec;
mod log_ext;

use clap::Clap;
use std::{
    error,
    fmt,
    io::{
        self,
        Write,
    },
    time::Duration,
};
use log::*;
use timeout::{
    FailableTimeoutError,
};

const DEFAULT_TIMEOUT: Duration = Duration::from_secs(5 * 60);

#[derive(Debug, Clap)]
#[clap(version = crate_version!(), author = crate_authors!())]
struct Options {
    #[clap(long, about = "Timeout for PID searching in seconds [default: 300]")]
    pid_search_sec: Option<u64>,
    #[clap(long, about = "Timeout for PID searching in seconds [default: 300]")]
    connect_timeout_sec: Option<u64>,
    #[clap(long, default_value = "1000", about = "Interval to wait in between attempts (in milliseconds)")]
    attempt_interval_ms: u64,
}

impl Options {
    #[inline(always)]
    fn pid_search_timeout(&self) -> Duration {
        self.pid_search_sec
            .map(Duration::from_secs)
            .unwrap_or(DEFAULT_TIMEOUT)
    }

    #[inline(always)]
    fn connect_timeout(&self) -> Duration {
        self.connect_timeout_sec
            .map(Duration::from_secs)
            .unwrap_or(DEFAULT_TIMEOUT)
    }

    #[inline(always)]
    fn attempt_interval(&self) -> Duration {
        Duration::from_millis(self.attempt_interval_ms)
    }
}

#[derive(Debug, Clone, Copy)]
struct LeagueContext {
    league_client_pid: u32,
    league_client_ux_pid: u32,
}

impl LeagueContext {
    pub fn new() -> io::Result<Option<Self>> {
        let client_pid = procfs::pidof("LeagueClient.exe")?;
        debug!("client: {:?}", &client_pid);
        let ux_pid = procfs::pidof("LeagueClientUx.exe")?;
        debug!("ux: {:?}", &ux_pid);
        let ret = client_pid
            .and_then(|client_pid| ux_pid.map(move |ux_pid| LeagueContext {
                league_client_pid: client_pid,
                league_client_ux_pid: ux_pid,
            }));
        Ok(ret)
    }

    fn app_port(&self) -> io::Result<Option<u32>> {
        use regex::Regex;
        let process = procfs::ProcfsProcess::new(self.league_client_ux_pid);
        let args = process.cmdline()?;
        let pat = Regex::new(r"^--app-port=([0-9]+)").unwrap();
        let port = args.into_iter()
            .filter_map(|s| pat.captures_iter(&s).next().and_then(|s| s[1].parse().ok()))
            .next();
        Ok(port)
    }

    #[inline(always)]
    fn ux_pid(&self) -> nix::unistd::Pid {
        use nix::unistd::Pid;
        Pid::from_raw(self.league_client_ux_pid as libc::pid_t)
    }

    fn stop_ux(&self) -> nix::Result<()> {
        use nix::{
            sys::signal::{
                self,
                Signal,
            },
        };
        signal::kill(self.ux_pid(), Some(Signal::SIGSTOP))
    }

    fn resume_ux(&self) -> nix::Result<()> {
        use nix::sys::signal::{
            self,
            Signal,
        };
        signal::kill(self.ux_pid(), Some(Signal::SIGCONT))
    }
}

fn can_connect(port: u32) -> io::Result<bool> {
    use std::process::{
        Command,
        Stdio,
    };

    let mut child = Command::new("/usr/bin/openssl")
        .arg("s_client")
        .arg("-connect")
        .arg(format!(":{}", port))
        .stdin(Stdio::piped())
        .stdout(Stdio::null())
        .spawn()?;

    {
        let stdin = child.stdin.as_mut().unwrap();
        stdin.write_all(b"Q")?;
    }

    let status = child.wait()?;
    Ok(status.success())
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct PortConnectionError(u32);

impl PortConnectionError {
    #[inline(always)]
    fn port(self) -> u32 {
        self.0
    }
}

impl fmt::Display for PortConnectionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error connecting to :{}", self.port())
    }
}

impl error::Error for PortConnectionError {
    #[inline(always)]
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Debug, thiserror::Error)]
enum ClientSuspendFixError {
    #[error("Error searching for client pids")]
    PidSearch(FailableTimeoutError<io::Error>),
    #[error("Error searching for app-port")]
    AppPortSearch(io::Error),
    #[error("Failed to find app port")]
    AppPortNotFound,
    #[error("Failed to stop LeagueClientUx")]
    StopUx(nix::Error),
    #[error("Timed out while waiting to be able to connect to client")]
    AppConnection(FailableTimeoutError<io::Error>),
    #[error("Failed to resume LeagueClientUx")]
    ResumeUx(nix::Error),
}

fn real_main(options: &Options) -> Result<(), ClientSuspendFixError> {
    use timeout::do_timeout_failable;

    let context = do_timeout_failable(options.attempt_interval(), options.pid_search_timeout(), LeagueContext::new)
        .map_err(ClientSuspendFixError::PidSearch)?;
    debug!("Context:\n{:?}", &context);
    let app_port = context.app_port()
        .map_err(ClientSuspendFixError::AppPortSearch)
        .and_then(|v| match v {
            Some(v) => Ok(v),
            None => Err(ClientSuspendFixError::AppPortNotFound),
        })?;
    info!("app_port: {:?}", &app_port);
    info!("Stopping UX. pid = {}", context.league_client_ux_pid);
    context.stop_ux()
        .map_err(ClientSuspendFixError::StopUx)?;

    do_timeout_failable(options.attempt_interval(), options.connect_timeout(), || {
        can_connect(app_port)
            .map(|success| {
                if !success {
                    info!("Connection failed to app port: {}", app_port);
                    None
                } else {
                    Some(())
                }
            })
    })
        .map_err(ClientSuspendFixError::AppConnection)?;

    info!("Connection success for app port: {}", app_port);
    info!("Resuming UX. pid = {}", context.league_client_ux_pid);
    context.resume_ux()
        .map_err(ClientSuspendFixError::ResumeUx)?;
    Ok(())
}

fn main() {
    // Initialize logging based on RUST_LOG env var
    log_ext::init_logging();

    let options = Options::parse();

    // Run program
    match real_main(&options) {
        Ok(..) => info!("done."),
        Err(e) => {
            error!("{}", &e);
            panic!("{}", e);
        },
    }
}
