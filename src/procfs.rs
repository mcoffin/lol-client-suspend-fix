use std::{
    io::{
        self,
        BufRead,
        BufReader,
        ErrorKind,
    },
    ffi,
    fs,
    path::{
        Path,
        PathBuf,
    },
    collections::HashMap,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ProcfsProcess {
    pid: u32,
}

#[inline(never)]
#[no_mangle]
fn strip_whitespace<'a>(orig: &'a str) -> &'a str {
    use crate::tailrec::{
        TailrecResult,
        tailrec,
    };
    tailrec(orig, |orig: &'a str| {
        match orig.strip_prefix(|c| c == '\t' || c == ' ') {
            Some(s) => TailrecResult::Continue(s),
            None => TailrecResult::Done(orig),
        }
    })
}

impl ProcfsProcess {
    #[inline]
    pub fn new(pid: u32) -> Self {
        ProcfsProcess {
            pid: pid,
        }
    }

    pub fn directory(&self) -> PathBuf {
        let mut p = PathBuf::from("/proc");
        p.push(format!("{}", self.pid));
        p
    }

    fn subdirectory<P: AsRef<Path>>(&self, subpath: P) -> PathBuf {
        let mut p = self.directory();
        p.push(subpath.as_ref());
        p
    }

    pub fn cmdline(&self) -> io::Result<Vec<String>> {
        let p = {
            let mut p = self.directory();
            p.push("cmdline");
            p
        };
        let f = fs::OpenOptions::new()
            .read(true)
            .open(&p)
            .map(BufReader::new)?;
        let cmdline = match f.lines().next().and_then(|v| v.ok()) {
            Some(v) => v,
            None => {
                return Ok(Vec::new());
            },
        };
        let ret = cmdline.split("\0")
            .map(String::from)
            .collect();
        Ok(ret)
    }

    pub fn stats(&self) -> io::Result<HashMap<String, String>> {
        let p = self.subdirectory("status");
        let f = fs::OpenOptions::new()
            .read(true)
            .open(p)
            .map(BufReader::new)?;
        f.lines()
            .map(|line| {
                let line = line?;
                let mut it = line.split(":").map(String::from);
                let key = it.next()
                    .map(Ok)
                    .unwrap_or_else(|| Err(io::Error::new(ErrorKind::Other, format!("Invalid input line for status"))))?;
                let mut is_first = true;
                let value = it.fold(String::new(), |a, b| {
                    if is_first {
                        is_first = false;
                        return b;
                    }
                    format!("{}:{}", a, b)
                });
                Ok((key, strip_whitespace(&value).to_string()))
            })
            .fold(Ok(HashMap::new()), |v, value| {
                let (mut v, (key, value)) = v.and_then(move |v| value.map(|value| (v, value)))?;
                v.insert(key, value);
                Ok(v)
            })
    }
}

trait StrStripExt {
    fn strip_suffix_optional<'a>(&'a self, suffix: &str) -> &'a Self;
}

impl StrStripExt for str {
    fn strip_suffix_optional<'a>(&'a self, suffix: &str) -> &'a Self {
        match self.strip_suffix(suffix) {
            Some(s) => s,
            None => self,
        }
    }
}

pub fn pidof<S: AsRef<ffi::OsStr>>(search: S) -> io::Result<Option<u32>> {
    use std::process::Command;
    use log::debug;
    debug!("running /usr/bin/pidof \"{:?}\"", search.as_ref());
    let output = Command::new("/usr/bin/pidof")
        .arg(search)
        .output()?;
    debug!("output: {}", std::str::from_utf8(output.stdout.as_slice()).unwrap());
    let output_s = std::str::from_utf8(output.stdout.as_slice()).ok();
    let ret = output_s
        .map(|s| s.strip_suffix_optional("\n"))
        .and_then(|s| s.parse::<u32>().ok());
    Ok(ret)
}
