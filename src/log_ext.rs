use std::{
    env,
    ffi,
    sync::Once,
};

static INIT_LOGGING: Once = Once::new();

pub const DEFAULT_LOG_LEVEL: &'static str = "info";

fn set_var_default<K, V>(k: K, v: V) where
    K: AsRef<ffi::OsStr>,
    V: AsRef<ffi::OsStr>,
{
    if env::var_os(k.as_ref()).is_none() {
        env::set_var(k, v);
    }
}

#[inline(never)]
pub fn init_logging() {
    INIT_LOGGING.call_once(|| {
        set_var_default("RUST_LOG", format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        env_logger::init();
    });
}
