use std::{
    error::Error,
    fmt,
    time::{
        Duration,
        Instant,
    },
    thread,
};

#[derive(Debug, Clone, Copy)]
struct SecondsDuration(Duration);

impl fmt::Display for SecondsDuration {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} seconds", self.0.as_secs())
    }
}

impl From<Duration> for SecondsDuration {
    #[inline(always)]
    fn from(d: Duration) -> Self {
        SecondsDuration(d)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct TimeoutError(Duration);

impl TimeoutError {
    #[inline(always)]
    pub fn duration(&self) -> Duration {
        self.0
    }
}

impl fmt::Display for TimeoutError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Timed out after duration: {}", SecondsDuration::from(self.duration()))
    }
}

impl Error for TimeoutError {}

impl From<Duration> for TimeoutError {
    #[inline(always)]
    fn from(d: Duration) -> Self {
        TimeoutError(d)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum FailableTimeoutError<E: Error> {
    #[error("Operation failed")]
    Failure(E),
    #[error("Operation timed out")]
    Timeout(TimeoutError),
}

#[allow(dead_code)]
pub fn do_timeout<F>(interval: Duration, timeout: Duration, mut f: F) -> bool where
    F: FnMut() -> bool,
{
    let start = Instant::now();
    let mut status = f();
    while !status && start.elapsed() < timeout {
        thread::sleep(interval);
        status = f();
    }
    status
}

#[allow(dead_code)]
pub fn do_timeout_opt<F, T>(interval: Duration, timeout: Duration, mut f: F) -> Option<T> where
    F: FnMut() -> Option<T>,
    T: Sized,
{
    let mut ret: Option<T> = None;
    let f = {
        let ret: &mut Option<T> = &mut ret;
        move || {
            *ret = f();
            ret.is_some()
        }
    };
    do_timeout(interval, timeout, f);
    ret
}

pub fn do_timeout_failable<F, T: Sized, E: Sized>(interval: Duration, timeout: Duration, mut f: F) -> Result<T, FailableTimeoutError<E>> where
    F: FnMut() -> Result<Option<T>, E>,
    E: Error,
{
    let mut f = move || f().map_err(FailableTimeoutError::Failure);
    let mut ret: Option<T> = f()?;
    let start = Instant::now();
    while ret.is_none() && start.elapsed() < timeout {
        thread::sleep(interval);
        ret = f()?;
    }
    ret
        .map(Ok)
        .unwrap_or_else(|| Err(TimeoutError::from(timeout)))
        .map_err(FailableTimeoutError::Timeout)
}
