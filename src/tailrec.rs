#[derive(Debug, Clone, Copy)]
pub enum TailrecResult<State, Ret> {
    Continue(State),
    Done(Ret),
}

impl<State, Ret> TailrecResult<State, Ret> {
    #[inline(always)]
    pub fn is_continue(&self) -> bool {
        match self {
            &TailrecResult::Continue(..) => true,
            _ => false,
        }
    }

    #[inline(always)]
    pub fn is_done(&self) -> bool {
        match self {
            &TailrecResult::Done(..) => true,
            _ => false,
        }
    }
}

pub fn tailrec<F, State, Ret>(mut state: State, mut f: F) -> Ret where
    F: FnMut(State) -> TailrecResult<State, Ret>,
    State: Copy,
{
    use TailrecResult::*;
    loop {
        match f(state) {
            Continue(new_state) => {
                state = new_state;
            },
            Done(ret) => {
                return ret;
            },
        }
    }
}
